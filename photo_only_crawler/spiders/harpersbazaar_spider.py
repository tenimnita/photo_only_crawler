# -*- coding: utf-8 -*-
import json
import re
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item
from photo_only_crawler.utils import remove_query_in_url

# same as cosmo
class HarpersbazaarSpider(scrapy.Spider):
    name = "harpersbazaar"
    allowed_domains = ["www.harpersbazaar.com"]

    start_urls = [
        'http://www.harpersbazaar.com/ajax/infiniteload/?id=310ca272-560e-4773-93f5-2a594b50b689&class=CoreModels%5Csections%5CSectionModel&viewset=homepage&page=1',
        'http://www.harpersbazaar.com/ajax/infiniteload/?id=6f396396-349b-48dd-b7d6-0538d1ba7895&class=CoreModels%5Csections%5CSectionModel&viewset=section&page=1',
        'http://www.harpersbazaar.com/ajax/infiniteload/?id=19b4acc4-9a97-4bcf-9cbd-c5312636aa34&class=CoreModels%5Csections%5CSectionModel&viewset=section&page=1',
        'http://www.harpersbazaar.com/ajax/infiniteload/?id=e73f6057-8527-490a-b4b7-84f7fbd64dfa&class=CoreModels%5Csections%5CSectionModel&viewset=section&page=1',
        'http://www.harpersbazaar.com/ajax/infiniteload/?id=9f071897-65f2-4466-9fe9-17f30418ff67&class=CoreModels%5Csections%5CSectionModel&viewset=section&page=1',
    ]

    def parse_json(self, response):
        json_text = response.xpath('//script[@id="json-ld"]/text()').extract_first()
        res = []
        if json_text:
            try:
                json_response = json.loads(json_text, strict=False)
            except:
                return res
            if 'image' in json_response and 'gallery' in json_response['image']:
                for d in json_response['image']['gallery']:
                    res.append(d['url'])
        return res

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        img_urls = []
        # first image
        first_img = response.xpath('//div[@class="content-lede-image-wrap"]/img/@data-src').extract_first()
        if first_img:
            first_img = remove_query_in_url(first_img)
            img_urls.append(first_img)
        # images in content
        img_urls.extend([remove_query_in_url(url) for url in response.xpath('//picture/img/@data-src').extract()])
        img_urls.extend(self.parse_json(response))
        # print img_urls
        img_urls = list(set(img_urls))
        print len(img_urls)
        item = Item(
            url=response.url,
            photos=img_urls
        )
        # print item
        yield item

    def parse(self, response):
        self.logger.info(u'parse list: {}'.format(response.url))
        has_article = False
        for url in response.xpath('//a[@class="full-item-title item-title"]/@href').extract():
            has_article = True
            article_url = urljoin(base=response.url, url=url)
            # print article_url
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': article_url
            })
            if not existed:
                yield scrapy.Request(url=article_url, callback=self.parse_item)
        if has_article:
            matched = re.match(ur'(.*)&page=(\d+)', response.url, re.IGNORECASE)
            if matched:
                next_url = u'{}&page={}'.format(matched.group(1), int(matched.group(2)) + 1)
                yield response.follow(url=next_url, callback=self.parse)
