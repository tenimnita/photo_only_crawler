# -*- coding: utf-8 -*-
import json
import re
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item


class PopSugarSpider(scrapy.Spider):
    name = "pop_sugar"
    allowed_domains = ["www.popsugar.com"]

    start_urls = [
        'https://www.popsugar.com/',
    ]

    def get_article_node(self, response):
        nodes = response.xpath('//article[@id]')
        if len(nodes) == 1:
            return nodes[0]
        else:
            return None

    def parse(self, response):
        self.logger.info(u'parse: {}'.format(response.url))
        for url in response.xpath('//a[@data-ga-category="nav"]/@href').extract():
            yield response.follow(url, callback=self.parse_list)

    def parse_list(self, response):
        self.logger.info(u'parse list: {}'.format(response.url))
        for url in response.xpath('//div[@class="post-title"]/a/@href').extract():
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': url
            })
            if not existed:
                yield response.follow(url=url, callback=self.parse_item)
        next_url = response.xpath('//a[@class="pager-next omniture-track active"]/@href').extract_first()
        yield response.follow(next_url, callback=self.parse_list)

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        article_node = self.get_article_node(response)
        if not article_node:
            self.logger.info('not article')
            return
        type = article_node.xpath('./@data-node-type').extract_first()
        if type == 'text':
            self.logger.info('type = text')
            img_urls = article_node.xpath('.//img/@src').extract()
            yield Item(
                url=response.url,
                photos=img_urls
            )
        elif type == 'gallery':
            self.logger.info('type = gallery')
            for node in response.xpath('//script[@type="application/ld+json"]/text()').extract():
                try:
                    json_node = json.loads(node)
                    if 'itemListElement' in json_node:
                        img_urls = []
                        for item in json_node['itemListElement']:
                            img_urls.append(item['item']['image'])
                        yield Item(
                            url=response.url,
                            photos=img_urls
                        )
                except:
                    pass
        else:
            self.logger.warning(u'Unknown type={}'.format(type))
