# -*- coding: utf-8 -*-
import json
import re
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item
from photo_only_crawler.utils import remove_query_in_url


class CosmopolitanSpider(scrapy.Spider):
    name = "cosmopolitan"
    allowed_domains = ["www.cosmopolitan.com"]

    start_urls = [
        'http://www.cosmopolitan.com/ajax/infiniteload/?id=d50755a3-19da-4033-ba32-379374bf8486&class=CoreModels%5Csections%5CSectionModel&viewset=homepage&page=1',
        'http://www.cosmopolitan.com/ajax/infiniteload/?id=73af4185-0d53-4e7f-bb34-3660c3b05f7b&class=CoreModels%5Csections%5CSubSectionModel&viewset=section&page=1',
        'http://www.cosmopolitan.com/ajax/infiniteload/?id=9f0a4e93-4d52-469a-9f80-bea698efba15&class=CoreModels%5Csections%5CSubSectionModel&viewset=section&page=1',
        'http://www.cosmopolitan.com/ajax/infiniteload/?id=02a7d69d-4dd1-4e41-90a9-144f071c735d&class=CoreModels%5Ccontenttypes%5CContentTypeModel&viewset=contenttype&page=1',
        'http://www.cosmopolitan.com/ajax/infiniteload/?id=28dea13e-2d00-4bb6-bbdd-47ea4295f7f9&class=CoreModels%5Ccollections%5CCollectionModel&viewset=collection&page=1'
    ]

    def parse_json(self, response):
        json_text = response.xpath('//script[@id="json-ld"]/text()').extract_first()
        res = []
        if json_text:
            try:
                json_response = json.loads(json_text, strict=False)
            except:
                return res
            if 'image' in json_response and 'gallery' in json_response['image']:
                for d in json_response['image']['gallery']:
                    res.append(d['url'])
        return res

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        img_urls = []
        # first image
        first_img = response.xpath('//div[@class="content-lede-image-wrap"]/img/@data-src').extract_first()
        if first_img:
            first_img = remove_query_in_url(first_img)
            img_urls.append(first_img)
        # images in content
        img_urls.extend([remove_query_in_url(url) for url in response.xpath('//picture/img/@data-src').extract()])
        img_urls.extend(self.parse_json(response))
        # print img_urls
        img_urls = list(set(img_urls))
        print len(img_urls)
        item = Item(
            url=response.url,
            photos=img_urls
        )
        # print item
        yield item

    def parse(self, response):
        self.logger.info(u'parse list: {}'.format(response.url))
        has_article = False
        for url in response.xpath('//a[@class="full-item-title item-title"]/@href').extract():
            has_article = True
            article_url = urljoin(base=response.url, url=url)
            # print article_url
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': article_url
            })
            if not existed:
                yield scrapy.Request(url=article_url, callback=self.parse_item)
        if has_article:
            matched = re.match(ur'(.*)&page=(\d+)', response.url, re.IGNORECASE)
            if matched:
                next_url = u'{}&page={}'.format(matched.group(1), int(matched.group(2)) + 1)
                yield response.follow(url=next_url, callback=self.parse)
