# -*- coding: utf-8 -*-
import json
import re
import urlparse
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item


class InstyleSpider(scrapy.Spider):
    name = "instyle"
    allowed_domains = ["www.instyle.com"]

    start_urls = [
        'http://www.instyle.com/fashion',
        'http://www.instyle.com/celebrity',
        'http://www.instyle.com/beauty',
        'http://www.instyle.com/hair',
        'http://www.instyle.com/reviews-coverage',
        'http://www.instyle.com/shopping',
        'http://www.instyle.com/inthemag',
        'http://www.instyle.com/how-tos'
    ]

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        img_url = []
        img_url.extend(response.xpath('//div[@id="content"]//div[@class="image-wrap"]/img/@src').extract())
        img_url.extend(response.xpath('//div[@id="content"]//img[@data-src]/@data-src').extract())
        img_url = list(set(img_url))
        img_url = [self.normalize_img_url(u) for u in img_url]
        item = Item(
            url=response.url,
            photos=img_url
        )
        # print item
        yield item

    def normalize_img_url(self, img_url):
        return urlparse.urljoin(img_url, urlparse.urlparse(img_url).path)

    def parse(self, response):
        self.logger.info(u'parse list: {}'.format(response.url))
        for url in response.xpath('//article//a/@href').extract():
            article_url = urljoin(base=response.url, url=url)
            # print article_url
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': article_url
            })
            if not existed:
                yield scrapy.Request(url=article_url, callback=self.parse_item)
        next_url = response.xpath('//li[@class="pager-next last"]/a/@href').extract_first()
        yield response.follow(url=next_url, callback=self.parse)
