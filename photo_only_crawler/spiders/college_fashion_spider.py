# -*- coding: utf-8 -*-
import json
import re
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item


class CollegeFashionSpider(scrapy.Spider):
    name = "college_fashion"
    allowed_domains = ["www.collegefashion.net"]

    start_urls = [
        'https://www.collegefashion.net',
        # 'https://www.collegefashion.net/fashion-tips/',
        # 'https://www.collegefashion.net/inspiration/'
    ]

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        for article_node in response.xpath('//div[@class="m-detail--body"]'):
            img_url = article_node.xpath('.//img/@src').extract()
            item = Item(
                url=response.url,
                photos=img_url
            )
            yield item

    def parse_api(self, response):
        self.logger.info(u'parse api: {}'.format(response.url))
        json_res = json.loads(response.body)

        # next url
        try:
            next_url = 'https://www.collegefashion.net/.api/stream/?moreResultsToken={}'.format(
                json_res['moreResultsToken'])
            yield scrapy.Request(
                url=next_url,
                callback=self.parse_api
            )
        except Exception as e:
            self.logger.exception(e)
        # get item urls
        for item in json_res['items']:
            article_url = urljoin(base=response.url, url=item['path'])
            # print article_url
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': article_url
            })
            if not existed:
                yield scrapy.Request(url=article_url, callback=self.parse_item)

    def parse(self, response):
        self.logger.info(u'parse home page: {}'.format(response.url))
        for node in response.xpath('//script[@type="application/json"]/text()').extract():
            try:
                node_json = json.loads(node)
                if 'type' in node_json and node_json['type'] == 'phxListHub':
                    # next page
                    try:
                        token = node_json['collection']['moreResultsToken']
                        url = 'https://www.collegefashion.net/.api/stream/?moreResultsToken={}'.format(token)
                        yield scrapy.Request(
                            url=url,
                            callback=self.parse_api
                        )
                    except Exception:
                        pass
                    for item in node_json['collection']['items']:
                        article_url = urljoin(base=response.url, url=item['path'])
                        print article_url
                        existed = my_mongo.item_collection.find_one({
                            'site': self.name,
                            'url': article_url
                        })
                        if not existed:
                            yield scrapy.Request(url=article_url, callback=self.parse_item)
            except Exception as e:
                self.logger.exception(e)
