# -*- coding: utf-8 -*-
import json
import re
import urlparse
from urlparse import urljoin

import scrapy

from photo_only_crawler import my_mongo
from photo_only_crawler.items import Item


class InstyleSpider(scrapy.Spider):
    name = "elle"
    allowed_domains = ["www.elle.com"]

    start_urls = [
        'http://www.elle.com/landing-feed/?template=homepage&landingTemplate=standard&pageNumber=1',
        'http://www.elle.com/landing-feed/?template=section&landingTemplate=standard&id=5&pageNumber=1',#fashion
        'http://www.elle.com/landing-feed/?template=section&landingTemplate=standard&id=3&pageNumber=1', #beauty
        'http://www.elle.com/landing-feed/?template=section&landingTemplate=standard&id=4&pageNumber=1', #culture
        'http://www.elle.com/landing-feed/?template=section&landingTemplate=standard&id=7&pageNumber=1', #life-love
    ]

    def parse_item(self, response):
        self.logger.info(u'parse item: {}'.format(response.url))
        json_text = response.xpath('//script[@type="application/ld+json"]/text()').extract_first()
        if json_text:
            article_json = json.loads(json_text, strict=False)
            article_type = article_json['@type']
            if article_type == 'Article':
                if 'image' in article_json:
                    image_type = type(article_json['image'])
                    # print image_type
                    if image_type is unicode or image_type is str:
                        img_urls = [article_json['image']]
                        self.logger.info(u'type 1')
                    elif image_type is dict:
                        img_urls = article_json['image']['@list']
                        self.logger.info('type 2')
                else:
                    # get 1 image
                    img_urls = response.xpath('//img[@class="lazy-image swap-image   zoomable   "]/@data-zoom').extract()
                    self.logger.info('type 4')
                    if len(img_urls) == 0:
                        raise Exception('no image')
            elif article_type == 'ItemList':
                img_urls = []
                for element in article_json['itemListELement']:
                    img_urls.append(element['item']['image'])
                self.logger.info('type 3')
            else:
                raise Exception('wrong article_type = {}'.format(article_type))
        item = Item(
            url=response.url,
            photos=img_urls
        )
        # print item
        yield item

    def normalize_img_url(self, img_url):
        return urlparse.urljoin(img_url, urlparse.urlparse(img_url).path)

    def parse(self, response):
        self.logger.info(u'parse list: {}'.format(response.url))
        has_article = False
        for url in response.xpath('//a[@class="landing-feed--story-title link link-txt"]/@href').extract():
            has_article = True
            article_url = urljoin(base=response.url, url=url)
            # print article_url
            existed = my_mongo.item_collection.find_one({
                'site': self.name,
                'url': article_url
            })
            if not existed:
                yield scrapy.Request(url=article_url, callback=self.parse_item)
        if has_article:
            matched = re.match(ur'(.*)&pageNumber=(\d+)', response.url, re.IGNORECASE)
            if matched:
                next_url = u'{}&pageNumber={}'.format(matched.group(1), int(matched.group(2)) + 1)
                yield response.follow(url=next_url, callback=self.parse)
