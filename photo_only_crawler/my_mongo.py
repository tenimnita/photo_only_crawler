import pymongo
from pymongo.mongo_client import MongoClient

client = MongoClient('localhost', 27017)  # default maxPoolSize = 100
db = client['only_photos']

item_collection = db['items']

item_collection.create_index([
    ('site', pymongo.ASCENDING),
], unique=False)

item_collection.create_index([
    ('site', pymongo.ASCENDING),
    ('url', pymongo.ASCENDING),
], unique=True)
