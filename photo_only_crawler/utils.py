import urlparse


def remove_query_in_url(img_url):
    return urlparse.urljoin(img_url, urlparse.urlparse(img_url).path)
