# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem

from photo_only_crawler import my_mongo
from datetime import datetime

class AddAdditionalFieldsPipeline(object):
    def process_item(self, item, spider):
        item['site'] = spider.name
        item['crawled_time'] = datetime.now()
        return item


class CheckMandatoryPipeline(object):
    def process_item(self, item, spider):
        if len(item['photos']) == 0:
            raise DropItem('Item has no photos')
        return item


class DumpPipeline(object):
    def process_item(self, item, spider):
        my_mongo.item_collection.insert_one(dict(item))
        spider.logger.info('saved')
        return item
